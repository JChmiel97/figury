package figury;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Trapez extends Figura
{
	public Trapez(Graphics2D buf, int del, int w, int h) 
	{
		super(buf, del, w, h);
		shape = new Polygon(new int[] {19,11,11,19}, new int[] {0,7,23,30}, 4);
		aft = new AffineTransform();
		area = new Area(shape);
	}
}
