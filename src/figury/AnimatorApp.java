package figury;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ActionEvent;

public class AnimatorApp extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JPanel contentPane;
	public static AnimPanel kanwa;
	public boolean up = true;
	public double scRatioHor = 1.0, scRatioVert = 1.0;
	int i = 0, zmiany = 0;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param delay 
	 */
	public AnimatorApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int ww = 450, wh = 300;
		setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(Color.BLACK);
		setMinimumSize(getSize());
		setTitle("Animacje");
		
		AnimPanel kanBuf = new AnimPanel();
		kanBuf.setBounds(1, 1,  432, 236);
		kanwa = kanBuf;
		contentPane.add(kanwa);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				kanwa.initialize();
			}
		});
		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(contentPane.getWidth()/2-120, kanwa.getHeight()+3, 100, 23);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				kanwa.addFig();
				if(i == 0)
				{
				kanwa.animate();
				}
				i++;
			}
		});
		contentPane.add(btnAdd);
		JButton btnAnimate = new JButton("Animate");
		btnAnimate.setBounds(contentPane.getWidth()/2+20, kanwa.getHeight()+3, 100, 23);
		btnAnimate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				kanwa.animate();
			}
		});
		contentPane.add(btnAnimate);
		contentPane.addComponentListener(new ComponentListener()		//skalowanie okna
		{
			@Override
			public void componentHidden(ComponentEvent arg0) {
			}
			
			@Override
			public void componentMoved(ComponentEvent arg0) {
			}

			@Override
			public void componentResized(ComponentEvent arg0) 
			{
					kanwa.setBounds(1, 1, contentPane.getWidth()-2, contentPane.getHeight()-25);
					kanwa.reinitAfterResize();
					btnAdd.setBounds(contentPane.getWidth()/2-120, kanwa.getHeight()+3, 100, 23);
					btnAnimate.setBounds(contentPane.getWidth()/2+20, kanwa.getHeight()+3, 100, 23);
			}

			@Override
			public void componentShown(ComponentEvent arg0) {
			}
		});
	}
}