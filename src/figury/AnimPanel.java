package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// bufor
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	static Graphics2D buffer;

	private int delay = 40;
	static Timer timer;
	
	public LinkedList<Figura> figury = new LinkedList<>();
	public Figura fig;
	private static int numer = 0;
	public static int width,height;

	public AnimPanel() 
	{
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
		
	}
	
	public void initialize() 
	{
		width = getWidth();
		height = getHeight();

		image = (BufferedImage) createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		buffer.setBackground(Color.WHITE);
	}
	
	public void reinitAfterResize() 
	{
		width = getWidth();
		height = getHeight();

		image = (BufferedImage) createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		buffer.setBackground(Color.WHITE);
		for(Figura element : figury)
		{
			element.changeParams(buffer, width, height);
		}
	}
	
	void addFig() 
	{
		numer = ThreadLocalRandom.current().nextInt(5);
		if (numer % 5 == 0)
			fig = new Kwadrat(buffer, delay, getWidth(), getHeight());
		if (numer % 5 == 1 )
			fig = new Elipsa(buffer, delay, getWidth(), getHeight());
		if (numer % 5 == 2)
			fig = new Trojkat(buffer, delay, getWidth(), getHeight());
		if (numer % 5 == 3)
			fig = new Trapez(buffer, delay, getWidth(), getHeight());
		if (numer % 5 == 4)
			fig = new Slimak(buffer, delay, getWidth(), getHeight());
		figury.add(fig);
		timer.addActionListener(figury.getLast());
		new Thread(figury.getLast()).start();
	}
	
	void animate() 
	{
		if (timer.isRunning()) 
		{
			timer.stop();
		} else {
			timer.start();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());
	}
}
