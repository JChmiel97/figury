package figury;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Slimak extends Figura
{
	public Slimak(Graphics2D buf, int del, int w, int h) 
	{
		super(buf, del, w, h);
		Polygon s = new Polygon();
		for(int i=0;i<360;i++)
		{
			double t = i/360.0;
			s.addPoint((int) (30*t*Math.cos(8*t*Math.PI)),(int) (30*t*Math.sin(8*t*Math.PI)));
		}
		shape = s;
		aft = new AffineTransform();
		area = new Area(shape);
	}
}