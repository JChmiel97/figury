package figury;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Trojkat extends Figura
{
	public Trojkat(Graphics2D buf, int del, int w, int h) 
	{
		super(buf, del, w, h);
		shape = new Polygon(new int[] {10,10,0}, new int[] {0,10,10}, 3);
		aft = new AffineTransform();
		area = new Area(shape);
	}

}